from math import sqrt
from vec import Vec
import orthogonalization

def orthonormalize(L):
    '''
    Input: a list L of linearly independent Vecs
    Output: A list T of orthonormal Vecs such that for all i in [1, len(L)],
            Span L[:i] == Span T[:i]
    '''
    def compute_norm(x):
        return sqrt(x*x)

    veclist = orthogonalization.orthogonalize(L)
    normlist = [compute_norm(x) for x in veclist] 
    return [veclist[i]/normlist[i] for i in range(len(L))]


def aug_orthonormalize(L):
    '''
    Input:
        - L: a list of Vecs
    Output:
        - A pair Qlist, Rlist such that:
            * coldict2mat(L) == coldict2mat(Qlist) * coldict2mat(Rlist)
            * Qlist = orthonormalize(L)
    '''
    def adjust(v, multipliers):
        return Vec(v.D, {i:v[i]*multipliers[i] for i in v.D})
    
    vstarlist, sigma_vecs = orthogonalization.aug_orthogonalize(L)

    #multiply sigma_vecs 
    normlist = [sqrt(v*v) for v in vstarlist]
    sigma_vecs = [adjust(v, normlist) for v in sigma_vecs]
    return (orthonormalize(vstarlist), sigma_vecs)
