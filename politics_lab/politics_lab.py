voting_data = list(open("voting_record_dump109.txt"))

## Task 1

def create_voting_dict():
    """
    Input: None (use voting_data above)
    Output: A dictionary that maps the last name of a senator
            to a list of numbers representing the senator's voting
            record.
    Example: 
        >>> create_voting_dict()['Clinton']
        [-1, 1, 1, 1, 0, 0, -1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, 1]

    This procedure should return a dictionary that maps the last name
    of a senator to a list of numbers representing that senator's
    voting record, using the list of strings from the dump file (strlist). You
    will need to use the built-in procedure int() to convert a string
    representation of an integer (e.g. '1') to the actual integer
    (e.g. 1).

    You can use the split() procedure to split each line of the
    strlist into a list; the first element of the list will be the senator's
    name, the second will be his/her party affiliation (R or D), the
    third will be his/her home state, and the remaining elements of
    the list will be that senator's voting record on a collection of bills.
    A "1" represents a 'yea' vote, a "-1" a 'nay', and a "0" an abstention.

    The lists for each senator should preserve the order listed in voting data. 
    """
    voting_dict = {}
    for entry in voting_data:
        da = entry.replace('\n', '')
        da = da.split()
        voting_dict[da[0]] = list(map(lambda x: int(x), da[3:]))
    return voting_dict
        
## Task 2

def policy_compare(sen_a, sen_b, voting_dict):
    """
    Input: last names of sen_a and sen_b, and a voting dictionary mapping senator
           names to lists representing their voting records.
    Output: the dot-product (as a number) representing the degree of similarity
            between two senators' voting policies
    Example:
        >>> voting_dict = {'Fox-Epstein':[-1,-1,-1,1],'Ravella':[1,1,1,1]}
        >>> policy_compare('Fox-Epstein','Ravella', voting_dict)
        -2
    """
    return sum([voting_dict[sen_a][i]*voting_dict[sen_b][i] for i in range(len(voting_dict[sen_a]))])

## Task 3

def most_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is most
            like the input senator (excluding, of course, the input senator
            him/herself). Resolve ties arbitrarily.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> most_similar('Klein', vd)
        'Fox-Epstein'

    Note that you can (and are encouraged to) re-use you policy_compare procedure.
    """
    vd_keys = list(voting_dict)
    vd_keys.remove(sen)
    similarities = {}
    for cur_sen in vd_keys:
        similarities[cur_sen] = policy_compare(sen, cur_sen, voting_dict)
        
    from operator import itemgetter
    sorted_similarities = sorted(similarities.items(), key=itemgetter(1))
    return sorted_similarities[-1][0]


## Task 4

def least_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is least like the input
            senator.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> least_similar('Klein', vd)
        'Ravella'
    """
    vd_keys = list(voting_dict)
    vd_keys.remove(sen)
    similarities = {}
    for cur_sen in vd_keys:
        similarities[cur_sen] = policy_compare(sen, cur_sen, voting_dict)
        
    from operator import itemgetter
    sorted_similarities = sorted(similarities.items(), key=itemgetter(1))
    return sorted_similarities[0][0]

   
    

## Task 5

most_like_chafee    = most_similar('Chafee', create_voting_dict())
least_like_santorum = least_similar('Santorum', create_voting_dict())


# Task 6

def find_average_similarity(sen, sen_set, voting_dict):
    """
    Input: the name of a senator, a set of senator names, and a voting dictionary.
    Output: the average dot-product between sen and those in sen_set.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> find_average_similarity('Klein', {'Fox-Epstein','Ravella'}, vd)
        -0.5
    """
    vd_keys = list(voting_dict)
    vd_keys.remove(sen)
    similarities = []
    for cur_sen in sen_set:
        similarities.append(policy_compare(sen, cur_sen, voting_dict))
        
    return sum(similarities)/float(len(similarities))


democrats = set()
for entry in voting_data:
    da = entry.replace('\n', '')
    da = da.split()
    if da[1] == 'D':
        democrats.add(da[0])

vd = create_voting_dict()
avg_vd = {sen:find_average_similarity(sen, democrats, vd) for sen in democrats}

from operator import itemgetter
most_average_Democrat = sorted(avg_vd.items(), key=itemgetter(1))[0][0]


# Task 7

def find_average_record(sen_set, voting_dict):
    """
    Input: a set of last names, a voting dictionary
    Output: a vector containing the average components of the voting records
            of the senators in the input set
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': [0,0,1]}
        >>> find_average_record({'Fox-Epstein','Ravella'}, voting_dict)
        [-0.5, -0.5, 0.0]
    """
    sz = len(sen_set)
    return [sum(tpl)/float(sz) for tpl in zip(*[voting_dict[x] for x in sen_set])]


average_Democrat_record = find_average_record(democrats,create_voting_dict())


# Task 8

def bitter_rivals(voting_dict):
    """
    Input: a dictionary mapping senator names to lists representing
           their voting records
    Output: a tuple containing the two senators who most strongly
            disagree with one another.
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': [0,0,1]}
        >>> bitter_rivals(voting_dict)
        ('Fox-Epstein', 'Ravella')
    """
    rivalries = {}
    for sen in voting_dict:
        vd_keys = list(voting_dict)
        vd_keys.remove(sen)
        similarities = {}
        for cur_sen in vd_keys:
            similarities[cur_sen] = policy_compare(sen, cur_sen, voting_dict)
            
        from operator import itemgetter
        sorted_similarities = sorted(similarities.items(), key=itemgetter(1))
        rivalries[sen] = sorted_similarities[0]
    
    worst_rivalry = [None, None, float('inf')]

    for sen,enemy in rivalries.items():
        if enemy[1] < worst_rivalry[2]:
            worst_rivalry = [sen, enemy[0], enemy[1]]


    return tuple(worst_rivalry[:2])
