# version code 988
# Please fill out this stencil and submit using the provided submission script.
import random
from itertools import combinations

from GF2 import one
from vecutil import list2vec
from independence import is_independent


## Problem 1
def randGF2(): return random.randint(0,1)*one

a0 = list2vec([one, one,   0, one,   0, one])
b0 = list2vec([one, one,   0,   0,   0, one])

def randGF2():
    return random.randint(0,1)*one

def gen_u():
    return list2vec([randGF2() for _ in range(6)])

def choose_secret_vector(s,t):
    u = gen_u()
    while a0*u!=s or b0*u!=t: 
        u = gen_u()
    return u

def gen_pairs():
    return [(gen_u(), gen_u()) for _ in range(4)]

def choose_independent_vectors():
    pairs = [(a0,b0)] + gen_pairs() 

    any3_independent = 0
    while 1:
        any3_independent = 0
        pairs = [(a0,b0)] + gen_pairs()
        for combo in combinations(pairs, 3):
            veclist = [v for pair in combo for v in pair]
            k = int(is_independent(veclist))
            if not k:
                break
            any3_independent += k
        if any3_independent == 10:
            return pairs

pairlist = choose_independent_vectors()

## Problem 2
# Give each vector as a Vec instance
secret_a0 = pairlist[0][0]
secret_b0 = pairlist[0][1]
secret_a1 = pairlist[1][0]
secret_b1 = pairlist[1][1]
secret_a2 = pairlist[2][0]
secret_b2 = pairlist[2][1]
secret_a3 = pairlist[3][0]
secret_b3 = pairlist[3][1]
secret_a4 = pairlist[4][0]
secret_b4 = pairlist[4][1]
