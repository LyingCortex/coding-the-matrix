Solutions to all the assignments (homeworks/labs) of Coursera's Coding the Matrix (https://class.coursera.org/matrix-001/class/index).

All solutions *HAVE PASSED* the grader.
